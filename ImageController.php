<?php
// src/Controller/ImageController.php

namespace AppController;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ImageController extends AbstractController
{
    /**
     * @Route("/", name="image_form")
     */
    public function index()
    {
        return $this->render('image/form.html.twig');
    }

    /**
     * @Route("/fetch-images", name="fetch_images")
     */
    public function fetchImages(Request $request)
    {
        $url = $request->request->get('url');
        $html = file_get_contents($url);

        $crawler = new Crawler($html);
        $images = $crawler->filter('img')->each(function (Crawler $node) {
            return $node->attr('src');
        });

        $totalSize = 0;
        foreach ($images as $image) {
            $ch = curl_init($image);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_NOBODY, true);
            curl_exec($ch);
            $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
            curl_close($ch);

            if ($size > 0) {
                $totalSize += $size;
            }
        }

        $totalSizeMb = $totalSize / 1024 / 1024;

        return $this->render('image/result.html.twig', [
            'images' => $images,
            'totalSize' => round($totalSizeMb, 2),
            'count' => count($images),
        ]);
    }
}